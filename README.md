# ⬢ i.j HTML5 template

![idotj homepage screenshots](screenshot-homePage.png "screenshot of i.j homepage in three color themes")

A free and open-source web template, useful for your personal, commercial or business website.

* Simple and clean design with 7 complete pre-built page examples

* Integrated lazy-loading

* Full Responsive (mobile, tablet and desktop)

* Developed with the last Bootstrap5 version (files included)

* Supports customized CSS variables

* Light, gray and dark theme

* Well documented and easy to modify

* Optimized for SEO and Accessibility (WCAG)

* PWA ready

This template was tested at the following sites:

| Test tool | Score |
| ------ | ------ |
| [Lighthouse Metrics](https://lighthouse-metrics.com) | 100 ~ 95 |
| [WebPageTest](https://www.webpagetest.org/) | A+ ~ A |
| [Yellow Lab Tools](https://yellowlab.tools) | A / 100 ~ 96 |
| [W3C markup validation](https://validator.w3.org) | Passed |

These results may vary depending on the content and your hosting setup.

Working version of this template at:  
<https://template.idotj.com>  

## 🗂️ User guide

If you want to compress/minify your JavaScript, CSS or images online here you have some free tools:

* **Compress images in new generation formats (avif)**  
<https://squoosh.app/>

* **Minify CSS files**  
<https://csscompressor.com/>

* **Compress/Minify JS files**  
<https://www.toptal.com/developers/javascript-minifier>

## 🌐 Browser support

i.j HTML template is supported on the latest versions of the following browsers:

- Chrome
- Firefox
- Edge
- Safari
- Brave
- Opera

## 🚀 Improve me

Feel free to add your features and improvements.

## ⚖️ License

GNU Affero General Public License v3.0
