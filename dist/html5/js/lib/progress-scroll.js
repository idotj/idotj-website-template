// -Progress scroll
const progressIndicator = document.getElementById("progress-scroll");
const animatedCircle = document.getElementById("progress-scroll-circle-stroke");
const progressAppears = 250;
let sHeight;

function trigger(eventName) {
  const event = new Event(eventName);
  window.dispatchEvent(event);
}


// Animate the circle stroke
function updateProgress(perc) {
  let circle_offset = 125 * perc;
  animatedCircle.style.strokeDashoffset = 125 - circle_offset;
}

// Get body and window heights
function setSizes() {
  const windowHeight = window.innerHeight;
  const bodyHeight = document.body.offsetHeight;
  sHeight = bodyHeight - windowHeight;
}

// Handler
function handler() {
  setSizes();
  trigger("scroll");
}

(function initiateScroll() {
  let events = ["DOMContentLoaded", "load", "resize"],
    top,
    perc;
  setSizes();

  // Scroll event
  window.addEventListener("scroll", () => {
    top = this.pageYOffset || document.documentElement.scrollTop;
    perc = Math.max(0, Math.min(1, top / sHeight));
    updateProgress(perc);
    if (
      top >= progressAppears &&
      !progressIndicator.classList.contains("progress-active")
    ) {
      progressIndicator.classList.add("progress-active");
    }
    if (
      top < progressAppears &&
      progressIndicator.classList.contains("progress-active")
    ) {
      progressIndicator.classList.remove("progress-active");
    }
  });
  events.map((event) => {
    window.addEventListener(event, handler);
  });

  // Go to top of page when clicked
  progressIndicator.addEventListener("click", () => {
    window.scrollTo(0, 0);
  });
})();
