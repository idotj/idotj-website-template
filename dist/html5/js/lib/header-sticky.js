// -Header sticky
const headerHeight = 70;

function checkScroll() {
  if (
    document.body.scrollTop > headerHeight ||
    document.documentElement.scrollTop > headerHeight
  ) {
    body.classList.add("sticky-active");
  } else {
    body.classList.remove("sticky-active");
  }
}

window.onscroll = function () {
  checkScroll();
};
