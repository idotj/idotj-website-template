// -Manages web color theme
const themeLightBtn = document.getElementById("theme-light");
const themeGrayBtn = document.getElementById("theme-gray");
const themeDarkBtn = document.getElementById("theme-dark");

// Check client config
if (savedThemeColor) {
  document.getElementById(savedThemeColor).classList.add("active");
} else {
  if (
    window.matchMedia &&
    window.matchMedia("(prefers-color-scheme: dark)").matches
  ) {
    themeDarkBtn.classList.add("active");
  } else {
    // by default
    themeLightBtn.classList.add("active");
  }
}

// Switch color theme
const changeColorTheme = function () {
  body.className = this.id;
  themeRemoveCss();
  this.classList.add("active");
  localStorage.setItem("theme-color", this.id);
};

function themeRemoveCss() {
  themeLightBtn.classList.remove("active");
  themeGrayBtn.classList.remove("active");
  themeDarkBtn.classList.remove("active");
}

themeLightBtn.onclick = changeColorTheme;
themeGrayBtn.onclick = changeColorTheme;
themeDarkBtn.onclick = changeColorTheme;
