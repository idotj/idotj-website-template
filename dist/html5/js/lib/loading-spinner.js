// Add/Remove EventListeners for loading spinner
(function manageSpinner() {
  const spinnerCSS = "loading-spinner";

  // Remove CSS class to container
  const removeSpinnerClass = (e) => {
    const spinnerContainer = e.closest(`.${spinnerCSS}`);
    if (spinnerContainer) {
      spinnerContainer.classList.remove(spinnerCSS);
    }
  };

  // Remove EventListener and CSS class to container
  const removeSpinner = (e) => {
    const target = e.target;
    const spinnerContainer = target.closest(`.${spinnerCSS}`);
    if (spinnerContainer) {
      spinnerContainer.classList.remove(spinnerCSS);
    }
    target.removeEventListener("load", removeSpinner);
    target.removeEventListener("error", removeSpinner);
  };

  // Add EventListener to images
  document
    .querySelectorAll(`.${spinnerCSS} > img, .${spinnerCSS} > picture > img`)
    .forEach((e) => {
      if (e.complete) {
        removeSpinnerClass(e);
      } else {
        e.addEventListener("load", removeSpinner);
        e.addEventListener("error", removeSpinner);
      }
    });
})();
