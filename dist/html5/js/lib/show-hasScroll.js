// -Detects end of scroll and toggle css class
(function manageScrollY() {
  const scrollCSS = "show-hasScroll";

  const checkInitialScrollTop = (e) => {
    if (
      e.offsetHeight + e.scrollTop >= e.scrollHeight - 1 &&
      e.scrollHeight > e.clientHeight
    ) {
      e.parentElement.classList.add("endScroll");
    }
  };

  const scrollingY = (e) => {
    const target = e.target;
    const scrollPosition = target.offsetHeight + target.scrollTop;
    if (
      scrollPosition >= target.scrollHeight ||
      scrollPosition == target.scrollHeight - 1
    ) {
      target.parentElement.classList.add("endScroll");
    } else {
      target.parentElement.classList.remove("endScroll");
    }
  };

  // Add EventListener to scroll container
  document
    .querySelectorAll(`.${scrollCSS} > div, .${scrollCSS} > ul`)
    .forEach((e) => {
      checkInitialScrollTop(e);
      e.addEventListener("scroll", scrollingY);
    });
})();
