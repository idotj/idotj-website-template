// -Show a dynamic back link to portfolio
let urlHistory = document.referrer;
let urlBrowser = window.location.href;
let backLink = parentDirectory(urlBrowser);
let backLinkContainer = document.getElementById("backLinkContainer");

// Get browser url and return it one level up
function parentDirectory(url) {
  if (url.slice(-1) === "/") {
    url = url.substring(0, url.length - 1);
    const lastSlash = url.lastIndexOf("/");
    if (lastSlash === -1) {
      return url;
    }
    if (lastSlash === 0) {
      return "/";
    }
    let parentUrl = url.substring(0, lastSlash);
    // If url hasn't a slash at the end, then add it
    return parentUrl.substr(-1) != "/" ? (parentUrl += "/") : parentUrl;
  } else {
    const lastSlash = url.lastIndexOf("/");
    if (lastSlash === -1) {
      return url;
    }
    if (lastSlash === 0) {
      return "/";
    }
    return url.substring(0, lastSlash);
  }
}

// Get the name of last directory in url
function parentDirectoryName(url) {
  const urlParts = url.split("/");
  // Handle potential trailing slash
  const name = urlParts.pop() || urlParts.pop();

  return name.charAt(0).toUpperCase() + name.slice(1);
}

// Inject back link in the container
if (backLinkContainer) {
  if (urlHistory === backLink) {
    backLinkContainer.innerHTML =
      'Back to <a onClick="javascript:history.go(-1)" class="link">Portfolio > ' +
      parentDirectoryName(backLink) +
      "</a>";
  } else {
    backLinkContainer.innerHTML =
      'Go to <a href="' +
      backLink +
      '" class="link">Portfolio > ' +
      parentDirectoryName(backLink) +
      "</a>";
  }
}
