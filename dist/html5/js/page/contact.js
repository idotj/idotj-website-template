// -Initialize Leaflet
let map = L.map("contactMap", {
  keyboard: false,
  zoomControl: true,
  scrollWheelZoom: false,
}).setView(
  {
    lon: 4.352540,
    lat: 50.846696
  },
  15
);

// Add the OpenStreetMap tiles
L.tileLayer("https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png", {
  maxZoom: 19,
  attribution:
    '&copy; <a href="https://www.openstreetmap.org/copyright" target="_blank" rel="noopener noreferrer">OpenStreetMap</a> contributors, Tiles style by <a href="https://www.hotosm.org/" target="_blank" rel="noopener noreferrer">Humanitarian OpenStreetMap Team</a> hosted by <a href="https://openstreetmap.fr/" target="_blank" rel="noopener noreferrer">OpenStreetMap France</a>',
}).addTo(map);

// Add tabindex="-1" to avoid keyboard focus
NodeList.prototype.forEach =
  NodeList.prototype.forEach || Array.prototype.forEach;
document.querySelectorAll("#contactMap a").forEach((x) => (x.tabIndex = -1));
